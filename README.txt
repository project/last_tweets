INTRODUCTION
------------

The Last Tweets module displays the last posted tweet for a given account.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/last_tweets

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/last_tweets

REQUIREMENTS
------------

This module requires the following PHP library: abraham/twitteroauth


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

 * composer require abraham/twitteroauth

CONFIGURATION
-------------

Configuration pth: /admin/config/last-tweets-settings
